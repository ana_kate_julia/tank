#pragma once

#include <vector>


template <typename T>
class IVelocity
{
public:
	virtual std::vector <T> getVel () = 0;
	virtual void setVel(std::vector <T> newValue) = 0;
};

 