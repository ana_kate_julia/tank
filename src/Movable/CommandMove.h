#pragma once

#include <vector>
#include "ICommand.h"
#include "IMovable.h"
#include <algorithm>

template <typename T>
class CommandMove : public ICommand
{
private:
	IMovable<T>& _moveAdapt;

public:
	CommandMove(IMovable<T>& moveAdapt) : _moveAdapt(moveAdapt) {}

	void execute()
	{
		std::vector<T>&& position = _moveAdapt.getPosition();
		const std::vector<T>&& velocity = _moveAdapt.getVelocity();

		if (position.size() != velocity.size()) {
			throw CommandException();
		}

		for (int i = 0; i < position.size(); i++)
		{
			position[i] += velocity[i];
		}

		_moveAdapt.setPosition(position);
	}
};