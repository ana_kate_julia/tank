#pragma once
#include <iostream>
#include <vector>
#include <exception>
#include "ICommand.h"
#include "IUObject.h"
#include "IVelocity.h"
#include "IRotable.h"

template <typename T>
class CommandVelocityCorrect : public ICommand {
private:
	IUObject& _obj;
	IVelocity<T>& _velocityAdapter;
	IRotable<T>& _rotateAdapter;
public:
	CommandVelocityCorrect(IUObject& obj, IVelocity<T>& velocityAdapter, IRotable<T>& rotateAdapter) :
		_obj(obj), _velocityAdapter(velocityAdapter), _rotateAdapter(rotateAdapter) {}

	void execute() {
		int radius = 0;
		std::vector<T> angle = _rotateAdapter.getAngle();
		std::vector<T> velocityCorrectable;
			for (T var : _velocityAdapter.getVel())
			{
				radius += var * var;
			}
	
		velocityCorrectable.push_back((T)(cos(_rotateAdapter.getAngle().at(0)) * sqrt(radius)));
		velocityCorrectable.push_back((T)(sin(_rotateAdapter.getAngle().at(0)) * sqrt(radius)));
		_velocityAdapter.setVel(velocityCorrectable);
		_rotateAdapter.setAngle(angle);
	}

	~CommandVelocityCorrect() = default;
};
