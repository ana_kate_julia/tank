#include <iostream>
#include "MovableTest/MockMovable.h"
#include "MovableTest/vectorOps.h"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
